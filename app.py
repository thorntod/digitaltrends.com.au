#!/usr/bin/env python3

import bcrypt
import json
import logging
import os
import secrets
import smtplib
import sqlite3
import string

from email.utils import formataddr
from flask import Flask, redirect, request, make_response, render_template, send_from_directory, session, url_for, g
from logging.handlers import RotatingFileHandler
from datetime import datetime
from dotenv import load_dotenv
from email.message import EmailMessage
from os import environ

# Load the variables fronm the .env file
load_dotenv()

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 300
app.config.from_pyfile('config.py')

SQLITE_DB = app.config['SQLITE_DB']

# Get email server properties
SMTP_SERVER     = environ.get('SMTP_SERVER')
SMTP_PORT       = environ.get('SMTP_PORT')
SMTP_USER       = environ.get('SMTP_USER')
SMTP_PASSWORD   = environ.get('SMTP_PASSWORD')


# ----------========== get_db ==========----------
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(SQLITE_DB)
        db.execute("PRAGMA foreign_keys = 1")
    db.row_factory = sqlite3.Row
    return db


# ----------========== close_connection ==========----------
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


# ----------========== query_db ==========----------
# Convenience funtion to handle the db call and closing cursor.
# the 'one' param is for if you only expect 1 row returned.
#
# eg: multiple rows
# for user in query_db('select * from users'):
#    print(user['username'], 'has the id', user['user_id'])
#
# eg: single result
# user = query_db('select * from users where username = ?',
#                [the_username], one=True)
# if user is None:
#    print('No such user')
# else:
#    print(the_username, 'has the id', user['user_id'])
#
def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


# ----------========== execute_db ==========----------
# Convenience funtion to handle the db call and closing cursor.
# Used generally for INSERT and UPDATE statements
#
def execute_db(query, args=()):
    cur = get_db().execute(query, args)
    cur.connection.commit()
    cur.close()


# ----------========== getCurrWeather ==========----------
@app.route('/getCurrWeather', methods=['GET', 'POST'])
def getCurrWeather():
    app.logger.debug('START: getCurrWeather')

    currWeatherFile = os.path.dirname(__file__) + '/static/json/bom_latest_observations.json'
    currWeatherJSON = {}

    with open(currWeatherFile, 'r') as f:
        currWeatherJSON = json.load(f)

    return currWeatherJSON


# ----------========== getUser ==========----------
def getUser(uid=None):
    app.logger.debug('START: getUser')

    returnDict = {}

    if uid is None:
        if 'uid' in session:
            uid = session['uid']
        else:
            return returnDict

    sql = 'SELECT uid, username, email, email_verified, change_pwd, create_date, last_login \
           FROM UserLogin \
           WHERE uid = ?'
    userLogin = query_db(sql, [uid, ], one=True)

    sql = 'SELECT * \
           FROM Users \
           WHERE uid = ?'
    user = query_db(sql, [uid, ], one=True)

    # Convert the sqlite <ROW> into a python dict
    returnDict = dict(user)
    # append
    returnDict.update(dict(userLogin))

    return returnDict


# ----------========== sendEmail ==========----------
def sendEmail(To, From="No-Reply<noreply@digitaltrends.com.au>", Subject="Default Subject", Message="Default Message"):
    app.logger.debug('START: sendEmail')

    msg = EmailMessage()

    msg.set_content(Message)

    msg['From']     = formataddr(('No-Reply', 'noreply@digitaltrends.com.au'))
    msg['To']       = To
    msg['Subject']  = Subject
    msg['Reply-To'] = formataddr(('No-Reply', 'noreply@digitaltrends.com.au'))

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(SMTP_USER, SMTP_PASSWORD)
    server.sendmail(From, To, msg.as_string())
    # server.send_message(msg, from_addrs=From)
    server.quit()
    app.logger.debug(msg.keys())
    app.logger.debug(msg['From'])

    # Dummy return code (success)
    return 0


# ----------========== generatePassword ==========----------
def generatePassword(length=8):
    app.logger.debug('START: generatePassword')

    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for i in range(length))

    return password


# ----------========== resetPassword ==========----------
@app.route("/resetPassword", methods=['POST'])
def resetPassword():
    app.logger.debug('START: resetPassword')

    if request.method == 'POST':

        # Create local variables for easy access
        email       = request.form['resetEmail']
        sql = 'SELECT uid \
               FROM UserLogin \
               WHERE lower(email) = lower(?)'
        userLogin = query_db(sql, [email, ], one=True)
        if userLogin:
            app.logger.debug('resetPassword: uid=' + str(userLogin['uid']))
            new_password = generatePassword()
            hashed_pwd = bcrypt.hashpw(new_password.encode("UTF-8"), bcrypt.gensalt())

            sql = 'UPDATE UserLogin \
                   SET password = ?, \
                   change_pwd = 1 \
                   WHERE uid = ?'
            execute_db(sql, [hashed_pwd, userLogin['uid'], ])

            email_message = 'Your password has been reset to: ' + new_password
            sendEmail(To = email, Subject = 'Password Reset', Message = email_message)

        else:
            return 'Email not found'

    return 'Reset email has been sent.'


# ----------========== updateLoginDate ==========----------
def updateLoginDate(uid=None):
    app.logger.debug('START: updateLoginDate')

    sql = 'UPDATE UserLogin \
           SET last_login = ? \
           WHERE uid = ?'
    update_vals = [datetime.now(), uid, ]
    execute_db(sql, update_vals)


# ----------========== 404 ==========----------
@app.errorhandler(404)
def page_not_found(e):
    # We show a catch all page with a link to the home page instead of having
    # the generic apache error screen.
    # note that we set the 404 return status explicitly

    return render_template('404.html'), 404


# ----------========== bookmarks ==========----------
@app.route("/bookmarks.html")
@app.route("/bookmarks")
def bookmarks():
    app.logger.debug('START: Bookmarks Page')

    return render_template('bookmarks.html', title='Bookmarks')


# ----------========== handy Links  ==========----------
@app.route("/handy-links")
def handy_links():
    app.logger.debug('START: Handy Links')

    return render_template('handy-links.html', title='Handy Links')


# ----------========== Glossary  ==========----------
@app.route("/glossary")
def glossary():
    app.logger.debug('START: Glossary')

    return render_template('glossary.html', title='Glossary')


# ----------========== Contact Us  ==========----------
@app.route("/contact-us")
def contact_us():
    app.logger.debug('Contact Us')

    return render_template('contact-us.html', title='Contact Us')


# ----------========== AFL Ladder ==========----------
@app.route("/afl_ladder")
def afl_ladder():
    app.logger.debug('START: AFL Ladder Page')

    return render_template('afl_ladder.html', title='AFL Ladder',
                           refresh=1800)


# ----------========== MotoGP Ladder ==========----------
@app.route("/motogp_ladder")
def motogp_ladder():
    app.logger.debug('START: MotoGP Ladder Page')

    return render_template('motogp_ladder.html', title='MotoGP Ladder',
                           refresh=1800)


# ----------========== dashboard ==========----------
@app.route("/dashboard")
def dashboard():
    app.logger.debug('START: Dashboard')

    user = getUser()

    if user['change_pwd']:
        message = 'Your password must be changed.'
        return redirect(url_for('change_password', message=message))

    return render_template('dashboard.html', title='Dashboard',
                           user=user)


# ----------========== logout ==========----------
@app.route("/logout")
def logout():
    app.logger.debug('START: Logout')

    session.clear()

    return redirect(url_for('home'))


# ----------========== login ==========----------
@app.route("/login", methods=['GET', 'POST'])
def login():
    app.logger.debug('START: Login Page')
    message = ''

    if request.method == 'POST':

        # Create local variables for easy access
        email       = request.form['email']
        password    = request.form['password']

        sql = 'SELECT * \
               FROM  UserLogin \
               WHERE lower(email) = lower(?) \
               OR    lower(username) = lower(?)'
        user = query_db(sql, [email, email, ], one=True)
        if user is None:
            message = 'No such user'
        else:
            message = email + ' has the id: ', user['uid']
            app.logger.debug(user['password'])

            if not bcrypt.checkpw(bytes(password, 'UTF-8'), user['password']):
                message = 'Password is wrong'
            else:
                session['uid'] = user['uid']
                updateLoginDate(user['uid'])

                return redirect(url_for('dashboard'))

    return render_template('login.html', title='Login', message=message)


# ----------========== google_login ==========----------
@app.route("/google_login", methods=['POST'])
def google_login():
    app.logger.debug('START: Google Login')

    email       = request.form['email']
    given_name  = request.form['given_name']
    family_name = request.form['family_name']
    picture     = request.form['picture']
    # sub         = request.form['sub']

    sql = 'SELECT * \
           FROM UserLogin \
           WHERE lower(email) = lower(?)'
    user = query_db(sql, [email, ], one=True)

    if user is None:
        password = generatePassword()

        cursor = get_db().cursor()
        sql = 'INSERT INTO UserLogin \
                 (email, create_date, last_login, password ) \
               VALUES \
                 (?, ?, ?, ?)'

        insert_vals = [email, datetime.now(), datetime.now(), password, ]
        cursor.execute(sql, insert_vals)
        cursor.connection.commit()
        new_uid = cursor.lastrowid

        sql = 'INSERT INTO Users \
                 (uid, given_name, family_name, picture ) \
               VALUES \
                 (?, ?, ?, ?)'
        insert_vals = [new_uid, given_name, family_name, picture, ]
        cursor.execute(sql, insert_vals)
        cursor.connection.commit()
        cursor.close()

        session['uid'] = new_uid

    else:
        session['uid'] = user['uid']
        updateLoginDate(user['uid'])

    return redirect(url_for('dashboard'))


# ----------========== change-password ==========----------
@app.route("/change-password", methods=['GET', 'POST'])
def change_password():
    app.logger.debug('START: Change Password')
    # app.logger.debug(request.args.get('message'))
    message = request.args.get('message')

    if request.method == 'POST':

        # Create local variables for easy access
        password1   = request.form['password1']
        password2   = request.form['password2']

        # Validate in case the form doesnt do it client side
        if password1.strip() == '':
            message = 'Password cannot be Empty'
        elif password1.strip() != password2.strip():
            message = 'Passwords must match'

        hashed_pwd = bcrypt.hashpw(password1.encode("UTF-8"), bcrypt.gensalt())
        # app.logger.debug(hashed_pwd)

        sql = 'UPDATE UserLogin \
               SET password = ?, \
               change_pwd = 0 \
               WHERE uid = ?'
        execute_db(sql, [hashed_pwd, session['uid'], ])

        return redirect(url_for('dashboard'))

    return render_template('change-password.html', title='Change Password', message=message)


# ----------========== signup ==========----------
@app.route("/signup", methods=['GET', 'POST'])
def signup():
    app.logger.debug('START: Signup Page')
    message = ''

    if request.method == 'POST':

        # Create local variables for easy access
        first_name  = request.form['firstName']
        last_name   = request.form['lastName']
        email       = request.form['email']
        password1   = request.form['password1']
        password2   = request.form['password2']

        # Validate in case the form doesnt do it client side
        if first_name.strip() == '':
            message = 'First Name cannot be Empty'
        elif last_name.strip() == '':
            message = 'Last Name cannot be Empty'
        elif email.strip() == '':
            message = 'Email cannot be Empty'
        elif password1.strip() == '':
            message = 'Password cannot be Empty'
        elif password1.strip() != password2.strip():
            message = 'Passwords must match'
        else:
            # Check if this email is already registered.
            sql = 'SELECT * \
                   FROM UserLogin \
                   WHERE lower(email) = lower(?)'
            user = query_db(sql, [email, ], one=True)

            if user:
                message = 'That email address already exists.'
            else:
                # insert UserLogin record, then Users details
                hashed_pwd = bcrypt.hashpw(password1.encode("UTF-8"), bcrypt.gensalt())
                # app.logger.debug(hashed_pwd)

                cursor = get_db().cursor()
                sql = 'INSERT INTO UserLogin \
                        (email, password, create_date, last_login ) \
                    VALUES \
                        (?, ?, ?, ?)'

                insert_vals = [email, hashed_pwd, datetime.now(), datetime.now(), ]
                cursor.execute(sql, insert_vals)
                cursor.connection.commit()
                new_uid = cursor.lastrowid
                # app.logger.debug(new_uid)

                sql = 'INSERT INTO Users \
                        (uid, given_name, family_name ) \
                       VALUES \
                        (?, ?, ?)'
                insert_vals = [new_uid, first_name, last_name, ]
                cursor.execute(sql, insert_vals)
                cursor.connection.commit()
                cursor.close()

                session['uid'] = new_uid

                return redirect(url_for('dashboard'))

    return render_template('signup.html', title='Signup', message=message)


# ----------========== home ==========----------
@app.route("/home")
@app.route("/index")
@app.route("/index.html")
@app.route("/")
def home():
    app.logger.debug('START: Home Page')

    return render_template('index.html', title='Home', refresh=600)


# ----------========== debug ==========----------
@app.route("/debug")
def debug():
    app.logger.debug('START: debug')

    dt_test = environ.get('DT_TEST')

    # sendEmail(To = 'david.thornton@digitaltrends.com.au', Subject = 'Test', Message = 'Testing 123')

    return render_template('debug.html', title='Debug', dt_test=dt_test,
                           user=getUser(), smtp_server=SMTP_SERVER,
                           password=generatePassword())


# ----------========== sitemap.xml  ==========----------
@app.route("/sitemap.xml")
def sitemap():
    app.logger.debug('sitemap.xml')

    resp = make_response(send_from_directory(app.static_folder, 'sitemap.xml'))
    resp.headers['Content-type'] = 'application/xml; charset=utf-8'
    return resp


# ----------========== robots.txt  ==========----------
@app.route("/robots.txt")
def robots():
    app.logger.debug('robots.txt')

    return send_from_directory(app.static_folder, 'robots.txt')


# ================== __main__ ====================
if __name__ == "__main__":
    LOG_FILENAME = app.config['LOG_FILENAME']
    LOG_LEVEL    = app.config['LOG_LEVEL']

    formatter = logging.Formatter("[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
    handler   = RotatingFileHandler(LOG_FILENAME, maxBytes=10000, backupCount=3)

    handler.setLevel(LOG_LEVEL)
    handler.setFormatter(formatter)

    app.logger.addHandler(handler)
    log = logging.getLogger('werkzeug')
    log.setLevel(LOG_LEVEL)
    log.addHandler(handler)

    # Assign server values
    FLASK_RUN_HOST = app.config['FLASK_RUN_HOST']
    FLASK_RUN_PORT = app.config['FLASK_RUN_PORT']
    DEBUG          = app.config['DEBUG']

    # Print out app.config values
    print()

    print('========== app.config ==========')
    for key, value in app.config.items():
        print(f"{key: <30} : {value}")

    print()

    print('========== Server Info ==========')
    print('FLASK_RUN_HOST : ' + app.config['FLASK_RUN_HOST'])
    print('FLASK_RUN_PORT : ' + str(app.config['FLASK_RUN_PORT']))
    print('DEBUG          : ' + str(app.config['DEBUG']))

    print()

    print('========== STARTING FLASK ==========')
    app.run(host=FLASK_RUN_HOST, port=FLASK_RUN_PORT, debug=DEBUG)
