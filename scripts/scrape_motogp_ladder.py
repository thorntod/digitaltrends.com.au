#!/usr/bin/env python

import json
import sys

from bs4 import BeautifulSoup
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

script_name = sys.argv[0]
print('START >> ' + script_name)
print('Start Time: ' + datetime.now().strftime('%d-%b-%y %H:%M:%S'))

# Get the current dir (important for cron jobs)
pwd = sys.path[0]

# source data URL
motogp_ladder_URL = "https://www.motogp.com/en/world-standing/2022/MotoGP/Championship"
# service = Service(pwd + '/chromedriver')
service = Service('/snap/bin/chromium.chromedriver')
options = webdriver.ChromeOptions()
options.add_argument("--no-sandbox")
options.add_argument("--headless")
options.add_argument("--start-maximized")
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--window-size=1024x768")
driver = webdriver.Chrome(service=service, options=options)
driver.get("https://www.motogp.com/en/world-standing/2022/MotoGP/Championship")

soup = BeautifulSoup(driver.page_source, "lxml")

soup_body = soup.body
# print(soup_body.prettify())
tables = soup.find_all("table")

table_wrapper = soup.find("div", {"class": "v-data-table__wrapper"})
ladder_table = table_wrapper.find("table")
table_body = ladder_table.find("tbody")
ladder_tr = table_body.find_all("tr")

ladder_list = []

for tr in ladder_tr:

    ladder_dict = {}

    pos_td = tr.find("td", {"class": "table_item position"})
    pos = pos_td.span.contents[0].string.strip()
    ladder_dict['pos'] = pos

    rider_td = tr.find("td", {"class": "table_item rider"})
    name    = rider_td.find("span", {"class": "name"}).contents[0]
    surname = rider_td.find("span", {"class": "surname"}).contents[0]
    number  = rider_td.find("span", {"class": "number"}).contents[0]
    ladder_dict['rider'] = surname + ', ' + name
    ladder_dict['number'] = number

    nation_td = tr.find("td", {"class": "table_item nation"})
    country = nation_td.find("span", {"class": "country"}).contents[0]
    ladder_dict['country'] = country

    team_td = tr.find("td", {"class": "table_item team"})
    team = team_td.contents[0]
    ladder_dict['team'] = team

    bike_td = tr.find("td", {"class": "table_item bike"})
    bike = bike_td.contents[0].string.strip()
    ladder_dict['bike'] = bike

    points_td = tr.find("td", {"class": "table_item points"})
    points = points_td.contents[0]
    ladder_dict['points'] = points

    # APPEND TO THE DICT
    ladder_list.append(ladder_dict)

# print (ladder_list[0])
print(*ladder_list, sep='\n')

json_str = json.dumps(ladder_list, indent=4)

# print(json_str)

with open(pwd + '/../static/json/motogp_ladder.json', 'w') as file:
    file.write(json_str)

print('Output written to: ' + pwd + '/../static/json/motogp_ladder.json')
print('End Time: ' + datetime.now().strftime('%d-%b-%y %H:%M:%S'))
print('END   << ' + script_name)
