#!/usr/bin/env python

import json
import requests
import sys

from bs4 import BeautifulSoup

print('START >> scrape_afl_ladder.py')

# Get the current dir (important for cron jobs)
pwd = sys.path[0]

# source data URL
afl_ladder_URL = "http://www.afl.com.au/ladder?Competition=1&ShowBettingOdds=0&Live=0"

r = requests.get(afl_ladder_URL)
html_doc = r.text
soup = BeautifulSoup(html_doc, 'html5lib')

soup_body = soup.body
soup_ladder = soup.find("table", {"class": "ladder__table"})
ladder_body = soup_ladder.find("tbody")
ladder_tr = ladder_body.find_all("tr")

ladder_list = []

for tr in ladder_tr:

    ladder_dict = {}

    pos_td = tr.find("td", {"class": "ladder__cell ladder__cell--pos"})
    if pos_td is None:
        continue

    pos = pos_td.contents[0].string.strip()
    ladder_dict['pos'] = pos

    club_td = tr.find("td", {"class": "ladder__cell ladder__cell--club"})
    ladder_dict['club'] = club_td.string

    played_td = club_td.find_next("td")
    ladder_dict['played'] = played_td.string

    points_td = tr.find("td", {"class": "ladder__cell ladder__cell--points"})
    ladder_dict['points'] = points_td.string

    percentage_td = tr.find("td", {"class": "ladder__cell ladder__cell--percentage"})
    ladder_dict['percentage'] = percentage_td.string

    won_td = percentage_td.find_next("td")
    ladder_dict['won'] = won_td.string

    lost_td = won_td.find_next("td")
    ladder_dict['lost'] = lost_td.string

    drawn_td = lost_td.find_next("td")
    ladder_dict['drawn'] = drawn_td.string

    pcnt_for_td = drawn_td.find_next("td")
    ladder_dict['percent_for'] = pcnt_for_td.string

    pcnt_against_td = pcnt_for_td.find_next("td")
    ladder_dict['percent_against'] = pcnt_against_td.string

    # APPEND TO THE DICT
    ladder_list.append(ladder_dict)

# print (ladder_list[0])
print(*ladder_list, sep='\n')

json_str = json.dumps(ladder_list, indent=4)

# print(json_str)

with open(pwd + '/../static/json/afl_ladder.json', 'w') as file:
    file.write(json_str)
print('END   << scrape_afl_ladder.py')
