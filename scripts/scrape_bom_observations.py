#!/usr/bin/env python

import ftplib
import json
import os
import sys
import tarfile

from datetime import datetime

print('START >> scrape_bom_observations.py')

# Get the current dir (important for cron jobs)
pwd = sys.path[0]

# source data URL
# bom_observations_anon_FTP_URL = "ftp://ftp.bom.gov.au/anon/gen/fwo/IDW60910.tgz"
bom_observations_anon_FTP_URL = "ftp.bom.gov.au"
bom_observations_tgz_file = 'IDW60910.tgz'

# Get local mod time for FTP file
try:
    local_mod_time = datetime.utcfromtimestamp(os.path.getmtime(bom_observations_tgz_file)).strftime('%Y%m%d%H%M%S')
except Exception:
    local_mod_time = '19700101000000000'

ftp = ftplib.FTP(bom_observations_anon_FTP_URL)
ftp.login()
ftp.cwd('anon/gen/fwo')
# dir_list = ftp.dir()
dir_list = ftp.dir(bom_observations_tgz_file)
print(dir_list)
bom_mod_time = ftp.sendcmd('MDTM ' + bom_observations_tgz_file).split()[1]

print('Local mod Time: ' + local_mod_time)
print('Bom mod time: ' + bom_mod_time)

if bom_mod_time > local_mod_time:
    ftp.retrbinary('RETR ' + bom_observations_tgz_file, open(bom_observations_tgz_file, 'wb').write)
    ftp.quit()

    tar = tarfile.open(bom_observations_tgz_file)
    bom_observations_json_file = tar.extractfile('IDW60910.94608.json').read()
    tar.close()

    bom_json = json.loads(bom_observations_json_file)
    header = bom_json['observations']['header']
    data = bom_json['observations']['data']
    print(json.dumps((data[0]), indent=4))

    with open(pwd + '/../static/json/bom_latest_observations.json', 'w') as file:
        file.write(json.dumps((data[0]), indent=4))
else:
    ftp.quit()
    print('Remote file has not been updated. Doing nothing.')

print('END   << scrape_bom_observations.py')
