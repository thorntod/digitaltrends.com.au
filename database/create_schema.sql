#!/usr/bin/env python

import sqlite3
import sys
sys.path.insert(0, '..')
from config import SQLITE_DB

print(SQLITE_DB)

print("Start - Create Tables")
conn = sqlite3.connect(SQLITE_DB)
print("Connected")


print("Creating table: UserLogin")
conn.execute(''' CREATE TABLE IF NOT EXISTS UserLogin (
        uid             INTEGER PRIMARY KEY AUTOINCREMENT,
        username        TEXT UNIQUE,
        email           TEXT UNIQUE,
        email_verified  INTEGER DEFAULT 0,
        password        BLOB,
        change_pwd      INTEGER DEFAULT 0,
        create_date     TEXT    NOT NULL,
        last_login      TEXT );
            ''')

print("Creating table: Users")
conn.execute(''' CREATE TABLE IF NOT EXISTS Users (
        uid             INTEGER NOT NULL,
        given_name      TEXT,
        family_name     TEXT,
        gender          TEXT,
        birth_day       TEXT,
        birth_month     TEXT,
        birth_year      TEXT,
        picture         TEXT,
        timezone        TEXT,
        FOREIGN KEY (uid) REFERENCES UserLogin(uid) );
            ''')


conn.commit()
conn.close()

print("End - Create Tables")
